/*
 * BMI.cpp
 *
 *  Created on: 22.04.2017
 *      Author: RENT
 */
#include "BMI.hpp"

plec BMI::getPlec() const
{
	return mPlec;
}

void BMI::setPlec(plec plec)
{
	mPlec = plec;
}

float BMI:: getWaga() const
{
	return mWaga;
}

void BMI::setWaga(float waga)
{
	mWaga = waga;
}
int BMI:: getWiek() const
{
	return mWiek;
}

void BMI::setWiek(int wiek)
{
	mWiek = wiek;
}

float BMI::getWzrost() const
{
	return mWzrost;
}

void BMI::setWzrost(float wzrost)
{
	mWzrost = wzrost;
}

float BMI::CalculateBMI ()
{
	float BMI = 0;
	BMI = getWaga() / (getWzrost() * getWzrost());
	return BMI;
}

float BMI:: CalculateEnergy ()
{
	float energy = 0;
	if (mPlec == kobieta)
	{
		energy = 655.1 + (9.567 * getWaga()) + (185 * getWzrost())
				- (4.68 * getWiek());
	}
	if (mPlec == mezczyzna)
	{
		energy = 66.47 + (13.7 * getWaga()) + (500 * getWzrost())
				- (6.76 * getWiek());
	}
		return energy;


}
