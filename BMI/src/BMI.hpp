/*
 * BMI.hpp
 *
 *  Created on: 22.04.2017
 *      Author: RENT
 */

#ifndef BMI_HPP_
#define BMI_HPP_

enum plec
{kobieta,
mezczyzna
};


class BMI
{
public:
	float mWaga;
	float mWzrost;
	int mWiek;
	plec mPlec;

	BMI (float waga, float wzrost,int wiek,plec plec)
	:mWaga(waga)
	,mWzrost(wzrost)
	,mWiek(wiek)
	,mPlec(plec)
	{
	}

plec getPlec() const;

void setPlec(plec plec);

float getWaga() const;

void setWaga(float waga);

int getWiek() const;

void setWiek(int wiek);

float getWzrost() const;

void setWzrost(float wzrost);

float CalculateBMI ();

float CalculateEnergy ();
};



#endif /* BMI_HPP_ */
