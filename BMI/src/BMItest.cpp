/*
 * BMItest.cpp
 *
 *  Created on: 22.04.2017
 *      Author: RENT
 */

#include "gtest/gtest.h"
#include "BMI.hpp"

TEST(BMItest, Constructor) {
      BMI a (53.3, 1.62, 26, kobieta);
      EXPECT_EQ(kobieta , a.getPlec());
      EXPECT_FLOAT_EQ(53.3 , a.getWaga());
      EXPECT_FLOAT_EQ(1.62 , a.getWzrost());
      EXPECT_EQ(26, a.getWiek());
}

TEST(BMItest, GetterSetter) {
      BMI b (53.3, 1.62, 26, kobieta);
      EXPECT_EQ(kobieta , b.getPlec());
      EXPECT_FLOAT_EQ(53.3 , b.getWaga());
      EXPECT_FLOAT_EQ(1.62 , b.getWzrost());
      EXPECT_EQ(26, b.getWiek());

b.setWiek (23);
b.setWzrost (1.90);
b.setWaga (60.5);
b.setPlec (mezczyzna);


      EXPECT_EQ(mezczyzna , b.getPlec());
      EXPECT_FLOAT_EQ(60.5 , b.getWaga());
      EXPECT_FLOAT_EQ(1.90 , b.getWzrost());
      EXPECT_EQ(23, b.getWiek());
}

TEST (BMItest, calcBMI)
{BMI c (53.3, 1.62, 26, kobieta);
EXPECT_NEAR(20.30, c.CalculateBMI(), 0.02);
}



int main (int argc, char **argv)
{
	::testing::InitGoogleTest(&argc, argv);

	return RUN_ALL_TESTS();
}
