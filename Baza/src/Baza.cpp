//============================================================================
// Name        : Baza.cpp
// Author      : 
// Version     :
// Copyright   : Your copyright notice
// Description : Hello World in C++, Ansi-style
//============================================================================

#include <iostream>
using namespace std;

class Poczatek
{
protected:
	int x;

public:
	void przedstawSie()
	{
		cout << "Jestem poczatek" << endl;
	}

	Poczatek():x(0)
	{
		cout << "konst poczatek" << endl;
	}

	~Poczatek()
	{
		cout << "destr poczatek" << endl;
	}

	void zmienX()
	{
		x = 1;
	}
	void wypiszX()
	{
		cout <<"poczatek x="<< x << endl;
	}
};

class Srodek: public Poczatek
{
protected:
	int x;

public:
	void przedstawSie()
	{
		cout << "Jestem srodek" << endl;
	}

	Srodek():x(10)
	{
		cout << "konst srodek" << endl;
	}

	~Srodek()
	{
		cout << "destr srodek" << endl;
	}
	void zmienX()
	{
		x = 20;
		Poczatek::x = 3;
	}

	void wypiszX()
	{
		cout <<"poczatek x="<<Poczatek::x<<"srodek x="<< x << endl;
	}
};

class Koniec: public Srodek
{
protected:
	int x;

public:
	void przedstawSie()
	{
		cout << "Jestem koniec" << endl;
	}

	Koniec():x(100)
	{
		cout << "konst koniec" << endl;
	}

	~Koniec()
	{
		cout << "destr koniec" << endl;
	}
	void zmienX ()
	{
		x = 102;
		Poczatek::x = 12;
		Srodek::Poczatek::x = 2;
	}
	void wypiszX()
	{
		cout <<"poczatek x="<<Poczatek::x<<"srodek x="<< Srodek::Poczatek::x<<"koniec x=" << x << endl;
	}
};



int main() {
Poczatek pocz;
Srodek srod;
Koniec koni;

pocz.przedstawSie();
srod.przedstawSie();
koni.przedstawSie();

pocz.wypiszX();
srod.wypiszX();
koni.wypiszX();

pocz.zmienX();
srod.zmienX();
koni.zmienX();

pocz.wypiszX();
srod.wypiszX();
koni.wypiszX();
	return 0;
}
