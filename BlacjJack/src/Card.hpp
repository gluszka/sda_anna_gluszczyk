/*
 * Card.hpp
 *
 *  Created on: 25.04.2017
 *      Author: RENT
 */

#ifndef CARD_HPP_
#define CARD_HPP_


class Card {
public:
	enum Colour {
		Kier = 0,
		Karo,
		Trefl,
		Pik,
		ColourEnd
	};

	enum Figure {
		FIGURE_2 = 0,
		FIGURE_3,
		FIGURE_4,
		FIGURE_5,
		FIGURE_6,
		FIGURE_7,
		FIGURE_8,
		FIGURE_9,
		FIGURE_10,
		FIGURE_J,
		FIGURE_Q,
		FIGURE_K,
		FIGURE_A,
		FigureEnd
	};

	Card()
	: mColour(ColourEnd)
	, mFigure(FigureEnd)
    , mValue(0)
	{
	}

	void setValues(Colour color, Figure figure)
		{
		mColour = color;
		mFigure = figure;
		mValue = convertFigureToValue(figure);
		}

	Colour getColour() const
	{
		return mColour;
	}

	Figure getFigure() const
	{
		return mFigure;
	}

	int getValue() const
	{
		return mValue;
	}

private:
	Colour mColour;
	Figure mFigure;
	int mValue;

	int convertFigureToValue(Figure figure)
	{
		int value = 0;

		switch(figure)
		{
		case FIGURE_2:
		case FIGURE_3:
		case FIGURE_4:
		case FIGURE_5:
		case FIGURE_6:
		case FIGURE_7:
		case FIGURE_8:
		case FIGURE_9:
		case FIGURE_10:
			value = 2 + figure;
			break;
		case FIGURE_J:
		case FIGURE_Q:
		case FIGURE_K:
			value = 10;
			break;
		case FIGURE_A:
			value = 11;
			break;
		default:
			value = 0;
			break;
		}

		return value;
	}
};

#endif /* CARD_HPP_ */





