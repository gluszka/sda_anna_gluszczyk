/*
 * CardGame_Test.hpp
 *
 *  Created on: 25.04.2017
 *      Author: RENT
 */



#include "gtest/gtest.h"
#include "Card.hpp"

TEST (CardGameTest,cardGetting)
{
	Card card;
	EXPECT_EQ(0, card.getValue());
	EXPECT_EQ(Card::ColourEnd, card.getColour());
	EXPECT_EQ(Card::FigureEnd, card.getFigure());
}

TEST (CardGameTest,cardSetValue)
{
	Card card;
	card.setValues(Card::Karo, Card::FIGURE_3);
	EXPECT_EQ(3, card.getValue());
	EXPECT_EQ(Card::Karo, card.getColour());
	EXPECT_EQ(Card::FIGURE_3, card.getFigure());
}

TEST (DeckCardTest,deckGetCard)
{
	Deck deck;
	Card card;
	for (int i=0;i<52; i++)
	{
	card =deck.getNextCard();
	}
	EXPECT_EQ(0, card.getValue());
	EXPECT_EQ(Card::ColourEnd, card.getColour());
	EXPECT_EQ(Card::FigureEnd, card.getFigure());
}
int main (int argc, char **argv)
{
	::testing::InitGoogleTest(&argc, argv);

	return RUN_ALL_TESTS();
}
