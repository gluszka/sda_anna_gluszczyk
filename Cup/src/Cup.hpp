/*
 * Cup.hpp
 *
 *  Created on: 08.05.2017
 *      Author: RENT
 */

#ifndef CUP_HPP_
#define CUP_HPP_
#include "Liquid.hpp"
#include <cstdlib>


class Cup

{
private:
	Liquid ** mContent;
	size_t mSize;

	void resize ()
	{
		if (mSize ==0)
		{
			mContent = new Liquid*[++mSize];
		}
		else
		{
			Liquid**tmp = new Liquid*[++mSize];

			for (size_t i = 0; i < mSize - 1; i++)
			{
				tmp[i] = mContent[i];
			}
			delete[]mContent;
			mContent = tmp;
		}
	}

public:
Cup()
:mContent(0), mSize(0)
	{
	}
	void wypisz()

	{
		for (size_t i = 0; i < mSize; i++)
		std::cout<<"["<<mContent[i]->getAmount()<<std::endl;
	}
void add (Liquid *liquid)
    {
    resize ();
    mContent[mSize-1]=liquid;
    }
//void add (const Milk& liquid)
//    {
//    resize ();
//    mContent[mSize-1]=new Milk liquid;
//    }
int calculateLiquidAmount()
	{
		int liquidAmount = 0;
		if (mSize == 0)
		{//Donothing
		}
		else
		{
			for (size_t i = 0; i < mSize; i++)
			{
				liquidAmount += mContent[i]->getAmount();
			}
		}
return liquidAmount;
	}

void spill ()
	    {
	    	for(size_t i = 0; i < mSize; i++)
	    	{
	    		mContent[i]->removeAll();
	    	}
	    	clear();
	    }
void clear()

		{
			for (size_t i = 0; i < mSize; i++)
			{
				delete mContent[i];
			}

				delete[] mContent;
				mContent =0;
				mSize = 0;
		}

	void takeSip(int amount)
	{
		float liquidAmount = calculateLiquidAmount();
		if (liquidAmount > 0 && amount > 0)
		{
			if (liquidAmount <= amount)
			{
				spill();
			}
			else
			{
				for (size_t i = 0; i < mSize; i++)
				{
					float liquidRatio = (float) mContent[i]->getAmount()/(float) liquidAmount;
					mContent[i]->remove(liquidRatio * amount);

				}

			}
		}

	}


};



#endif /* CUP_HPP_ */
