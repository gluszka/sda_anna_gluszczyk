//============================================================================
// Name        : Cup.cpp
// Author      : 
// Version     :
// Copyright   : Your copyright notice
// Description : Hello World in C++, Ansi-style
//============================================================================

#include <iostream>
using namespace std;
#include "Liquid.hpp"
#include "Cup.hpp"
#include "Milk.hpp"
#include "Coffee.hpp"
#include "Rum.hpp"
#include "Espresso.hpp"
#include "Decaff.hpp"
int main() {
Cup kubekKawyzMlekiem;
kubekKawyzMlekiem.add(new Decaff(250));
kubekKawyzMlekiem.add(new Milk(100,2.0));
kubekKawyzMlekiem.wypisz();
kubekKawyzMlekiem.takeSip(50);
kubekKawyzMlekiem.wypisz();
	return 0;
}
