#include "Data.hpp"

Data::Data ()
:mDzien (0)
,mMiesiac(0)
,mRok(0)
{
}

Data::Data (unsigned int dzien ,unsigned int miesiac, unsigned int rok)

 {
setDzien(dzien);
setMiesiac(miesiac);
setRok(rok);
 }

 unsigned int Data::getDzien ()

 {
   return mDzien;
 }

 void Data::setDzien (unsigned int dzien)

 {
   if (dzien <1)
     mDzien =1;
   else if (dzien >31)
     mDzien = 31;
   else
     mDzien = dzien;
 }

 unsigned int Data::getMiesiac ()

 {
   return mMiesiac;
 }

 void Data::setMiesiac (unsigned int miesiac)

 {
   if (miesiac <1)
         mMiesiac =1;
       else if (miesiac >12)
         mMiesiac = 12;
       else
         mMiesiac = miesiac;
 }

 unsigned int Data::getRok ()

 {
   return mRok;
 }

 void Data::setRok (unsigned int rok)

{
	if (rok < 1900)
		mRok = 1900;
	else if (rok > 2017)
		mRok = 2017;
	else
		mRok = rok;
}

void Data::wypisz()
{
	cout << "dzien:" << mDzien << " " << "miesiac;" << mMiesiac << " " << "rok:"
			<< mRok << endl;
}

 void Data::przesunRok (unsigned int delta)
{
 mRok += delta;
}

 void Data::przesunMiesiac (unsigned int delta)
 {
  unsigned int nowyMiesiac = mMiesiac + delta - 1;
   mMiesiac = nowyMiesiac % 12;
   unsigned int deltaRok = (nowyMiesiac - mMiesiac) / 12;
   mMiesiac += 1;
   mRok += deltaRok;
 }

void Data::przesunDzien (unsigned int delta)
 {
 unsigned int nowyDzien = mDzien +delta -1;
 mDzien = nowyDzien %31;
 int deltaMiesiac = (nowyDzien -mDzien)/31;
 mDzien += 1;
 int nowyMiesiac = mMiesiac+ deltaMiesiac -1;
 mMiesiac = nowyMiesiac %12;
 unsigned int deltaRok = (nowyMiesiac - mMiesiac) / 12;
 mMiesiac += 1;
 mRok += deltaRok;
 }

Data Data::roznica(Data d)
{
  Data moja(mDzien, mMiesiac, mRok);
  int dni = konwertujDoLiczby(moja);
  int dniD = konwertujDoLiczby(d);
  int roznicaDnia = dni-dniD;
  return konwertujDoDaty(roznicaDnia);
}

int Data::konwertujDoLiczby(Data d)
{
 //todo dla g
  return 0;
}

Data Data::konwertujDoDaty(int liczbaDni)
{
 //todo dla g
  return Data(0,0,0);
}
