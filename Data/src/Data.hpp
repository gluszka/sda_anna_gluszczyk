/*
 * Data.hpp
 *
 *  Created on: 25.04.2017
 *      Author: RENT
 */

#ifndef DATA_HPP_
#define DATA_HPP_


#include <iostream>
#include <string>
#include <cmath>
using namespace std;

class Data
{

 private:
 unsigned int mDzien;
 unsigned int mMiesiac;
 unsigned int mRok;
 public:

 Data ();
 Data (unsigned int dzien ,unsigned int miesiac, unsigned int rok);
 unsigned int getDzien ();
 void setDzien (unsigned int dzien);
 unsigned int getMiesiac ();
 void setMiesiac (unsigned int miesiac);
 unsigned int getRok ();
 void setRok (unsigned int rok);
 void wypisz ();
 void przesunRok (unsigned int delta);
 void przesunMiesiac (unsigned int delta);
 void przesunDzien (unsigned int delta);
 int konwertujDoLiczby(Data data);

 Data konwertujDoDaty(int liczbaDni);
 Data roznica(Data d);

};





#endif /* DATA_HPP_ */
