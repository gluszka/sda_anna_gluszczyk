#include "Osoba.hpp"

Osoba::Osoba()
:mImie("")
,mNazwisko("")
,mPesel(0)
,mDataUrodzenia(0,0,0)
,mPlec(Osoba::kobieta)
{
}

Osoba::Osoba(std::string imie, std::string nazwisko, int *pesel, Data dataUrodzenia, Plec plec)
:mImie(imie),
mNazwisko(nazwisko),
mPesel(pesel),
mDataUrodzenia(dataUrodzenia),
mPlec(plec)
{
}

Osoba::Osoba(std::string imie, std::string nazwisko, int *pesel,Plec plec)
:mImie(imie)
,mNazwisko(nazwisko)
,mPlec(plec)
{
  setPesel(pesel);
}

Osoba::Osoba (Data dataUrodzenia)
: mDataUrodzenia (dataUrodzenia)
{
}

const Data& Osoba::getDataUrodzenia() const
{
    return mDataUrodzenia;
}

const std::string& Osoba::getImie() const
{
    return mImie;
}

void Osoba::setImie(const std::string& imie)
{
    mImie = imie;
}

const std::string& Osoba::getNazwisko() const
{
    return mNazwisko;
}

void Osoba::setNazwisko(const std::string& nazwisko)
{
    mNazwisko = nazwisko;
}

int* Osoba::getPesel() const
{
    return mPesel;
}

std::string Osoba::getPeselTekst()
{
	if (mPesel != 0)
	{
		std::string tmp;
		for (int i = 0; i < 11; i++)
		{
			tmp += '0' + mPesel[i];
		}
		return tmp;
	}
	return "";
}

void Osoba::setPesel(int* pesel)
{
	if (walidacja(pesel))
	{
		mPesel = pesel; //zapisujemy numer pesel do pola klasy
		int dzien, miesiac, rok;
		rok = 1900 + 10 * pesel[0] + pesel[1];
		miesiac = 10 * pesel[2] + pesel[3];
		dzien = 10 * pesel[4] + pesel[5];
		mDataUrodzenia = Data(dzien, miesiac, rok);
	}
	else
	{
		for (int i = 0; i < 11; i++)
		{
			mPesel[i] = 0;
		}
	}
}

Osoba::Plec Osoba::getPlec() const
{
  return mPlec;
}

void Osoba::setPlec(Plec plec)
{
    mPlec = plec;
}

bool Osoba::walidacja(int* pesel)
{
  return true;
}

bool Osoba::czyStarsza (Osoba o) //czy o jest starsza od this//
{
  Data oData = o.getDataUrodzenia ();
  if (mDataUrodzenia.getRok () < oData.getRok ())
    {
      return false;
    }
  else if (mDataUrodzenia.getRok () > oData.getRok ())
    {
      return true;
    }
  if (mDataUrodzenia.getMiesiac () < oData.getMiesiac ())
    {
      return false;
    }
  else if (mDataUrodzenia.getMiesiac () > oData.getMiesiac ())
    {
      return true;
    }
  if (mDataUrodzenia.getDzien () < oData.getDzien ())
    {
      return false;
    }
  else if (mDataUrodzenia.getDzien () < oData.getDzien ())
    {
      return true;
    }
  return false;

}
