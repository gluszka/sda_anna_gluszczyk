/*
 * Osoba.hpp
 *
 *  Created on: 25.04.2017
 *      Author: RENT
 */


#ifndef OSOBA_HPP_
#define OSOBA_HPP_
#include <string>
#include "Data.hpp"

class Osoba
{
public:
    enum Plec
    {
        kobieta,
        mezczyzna
    };

   Osoba();
    Osoba (std::string imie, std::string nazwisko, int *pesel, Data dataUrodzenia, Plec plec);
    Osoba (std::string imie, std::string nazwisko, int *pesel, Plec plec);
    Osoba (Data dataUrodzenia);
    const Data& getDataUrodzenia() const;
    const std::string& getImie() const;
    void setImie(const std::string& imie);
    const std::string& getNazwisko() const;
    void setNazwisko(const std::string& nazwisko);
    int* getPesel() const;
    std::string getPeselTekst();
    void setPesel(int* pesel);
    Plec getPlec() const;
    void setPlec(Plec plec);
    bool walidacja(int *pesel);
    bool czyStarsza (Osoba o);

private:

    std::string mImie;
    std::string mNazwisko;
    int *mPesel;
    Data mDataUrodzenia;
    Plec mPlec;
};

#endif /* OSOBA_HPP_ */

