/*
 * AbstrakcyjnaFabryka.hpp
 *
 *  Created on: 08.04.2017
 *      Author: RENT
 */

#ifndef ABSTRAKCYJNAFABRYKA_HPP_
#define ABSTRAKCYJNAFABRYKA_HPP_
#include <iostream>
using namespace std;
#include "Procesor.hpp"
#include "Cooler.hpp"

class AbsFactory

{
	public:
	virtual Procesor* createProcesor ()= 0;
	virtual Cooler* createCooler () =0;

	virtual ~AbsFactory ()
	{
	}

};

class IntelFactory :public AbsFactory
{
	private:
		int mSerialCounter;
	public:
		IntelFactory()
	{
			mSerialCounter = 0;
	}

	Procesor* createProcesor ()
	{
		mSerialCounter++;
		return new ProcesorIntel(mSerialCounter);
	}
	Cooler* createCooler ()
	{
		return new CoolerIntel();
	}
};


class AMDFactory :public AbsFactory
{
private:
	int mSerialCounter;
public:
	AMDFactory ()
{
		mSerialCounter =0;
}
	Procesor* createProcesor ()
	{
		mSerialCounter += 10;
		return new ProcesorAMD(mSerialCounter);
	}

    Cooler* createCooler ()
{
    	return new CoolerAMD();
}
	};

#endif /* ABSTRAKCYJNAFABRYKA_HPP_ */
