/*
 * Computer.hpp
 *
 *  Created on: 08.04.2017
 *      Author: RENT
 */

#ifndef COMPUTER_HPP_
#define COMPUTER_HPP_
#include <iostream>
using namespace std;
#include "Cooler.hpp"
#include "Procesor.hpp"
#include "AbstrakcyjnaFabryka.hpp"

class Computer
{
string mName;
Procesor* mProcesor;
Cooler* mCooler;

public:
Computer (string name, AbsFactory&factory)
:mName(name)
{
mProcesor = factory.createProcesor();
mCooler = factory.createCooler();
}

~Computer ()
{
delete mProcesor;
delete mCooler;
}

void run()
	{
	cout<<"nazywam sie "<<mName<< endl;
	mProcesor->process();
	mCooler->cool();
	}

};




#endif /* COMPUTER_HPP_ */
