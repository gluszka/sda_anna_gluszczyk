/*
 * Procesor.hpp
 *
 *  Created on: 08.04.2017
 *      Author: RENT
 */

#ifndef PROCESOR_HPP_
#define PROCESOR_HPP_
#include <iostream>
using namespace std;


class Procesor
{
protected:
	int mSerianumer;
public:
	virtual void process ()=0;

	Procesor(int number)
	:mSerianumer(number)
	{
	}
	virtual ~Procesor()
	{

	}
};
class ProcesorAMD :public Procesor
{
public:
	void process ()
	{
		cout<<"procesor AMD nr "<<mSerianumer<<" -pracuje"<<endl;
	}

	ProcesorAMD(int number)
	:Procesor(number)
	{

	}
	~ProcesorAMD()
	{

	}
};

class ProcesorIntel: public Procesor
{
public:
	void process ()
	{
		cout<<"procesor Intel nr"<<mSerianumer<<" -pracuje"<<endl;
	}
	ProcesorIntel(int number)
		:Procesor(number)
			{

			}
	~ProcesorIntel()
		{

		}
};

#endif /* PROCESOR_HPP_ */
