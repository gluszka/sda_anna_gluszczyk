//============================================================================
// Name        : Fabryka.cpp
// Author      : 
// Version     :
// Copyright   : Your copyright notice
// Description : Hello World in C++, Ansi-style
//============================================================================

#include <iostream>
using namespace std;
#include "Procesor.hpp"
#include "AbstrakcyjnaFabryka.hpp"
#include "Cooler.hpp"
#include "Computer.hpp"


int main() {
	IntelFactory intelFactory;
	AMDFactory amdFactory;

	Computer intelPC1 ("PC1", intelFactory);
	Computer amdPC1 ("PC2", amdFactory);
	Computer intelPC2("PC3", intelFactory);
	Computer amdPC2("PC4", amdFactory);
	Computer intelPC3("PC5", intelFactory);
	Computer amdPC3("PC6", amdFactory);

	intelPC1.run();
	amdPC1.run();
	intelPC2.run();
	amdPC2.run();
	intelPC3.run();
	amdPC3.run();

	return 0;
}
