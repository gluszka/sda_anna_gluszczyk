/*
 * A.cpp
 *
 *  Created on: 26.04.2017
 *      Author: RENT
 */

#include "A.hpp"
#include "B.hpp"

#include <iostream>

using namespace std;

A::A(int val)
:_val(val)
{
}
A::~A(){std::cout<<"~A"<<std::endl;}
void A::SetB(B *b)
{
    _b = b;
    cout<<"Inside SetB()"<<endl;
    _b->Print();
}

void A::Print()
{
    cout<<"Type:A val="<<_val<<endl;
}



