/*
 * A.hpp
 *
 *  Created on: 26.04.2017
 *      Author: RENT
 */

#ifndef A_HPP_
#define A_HPP_


class B;
class A
{
    int _val;
    B* _b;
public:

    A(int val);
   ~A();
    void SetB(B *b);
    void Print();
};






#endif /* A_HPP_ */
