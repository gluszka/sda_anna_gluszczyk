/*
 * B.cpp
 *
 *  Created on: 26.04.2017
 *      Author: RENT
 */

#include "B.hpp"
#include "A.hpp"
#include <iostream>

using namespace std;

B::B(double val)
:_val(val)
{
}
B::~B(){std::cout<<"~B"<<std::endl;}
void B::SetA(A *a)
{
    _a = a;
    cout<<"Inside SetA()"<<endl;
    _a->Print();
}

void B::Print()
{
    cout<<"Type:B val="<<_val<<endl;
}



