/*
 * B.hpp
 *
 *  Created on: 26.04.2017
 *      Author: RENT
 */

#ifndef B_HPP_
#define B_HPP_

class A;
class B
{
    double _val;
    A* _a;
public:

    B(double val);
    ~B();
    void SetA(A *a);
    void Print();
};



#endif /* B_HPP_ */
