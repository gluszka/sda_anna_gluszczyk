/*
 * CStrzelec.hpp
 *
 *  Created on: Mar 25, 2017
 *      Author: orlik
 */

#ifndef CSTRZELEC_HPP_
#define CSTRZELEC_HPP_

#include "CJednostka.hpp"

class CStrzelec : public CJednostka
{
public:
	CStrzelec(char symbol ='$', int zycie=10, int atak=10, int szybkosc=10);

	bool atak(int x, int y);
};


#endif /* CSTRZELEC_HPP_ */
