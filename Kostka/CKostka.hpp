/*
 * CKostka.hpp
 *
 *  Created on: Mar 23, 2017
 *      Author: orlik
 */

#ifndef CKOSTKA_HPP_
#define CKOSTKA_HPP_

class CKostka{
private:
	unsigned int mIloscScianek;
	unsigned int mWartosc;

public:
	CKostka(unsigned int iloscScianek);

	void losuj();
	unsigned int getWartosc();
};

#endif /* CKOSTKA_HPP_ */
