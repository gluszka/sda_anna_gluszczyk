/*
 * Kostkatest.cpp
 *
 *  Created on: 22.04.2017
 *      Author: RENT
 */
#include "gtest/gtest.h"
#include "Kostka1.cpp"

TEST(Kostka, test){
	Kostka k (6);
	EXPECT_LE (k.losuj(),6);
	EXPECT_GE (k.losuj(),1);
}

int main (int argc, char **argv)
{
	::testing::InitGoogleTest(&argc, argv);

	return RUN_ALL_TESTS();
}
