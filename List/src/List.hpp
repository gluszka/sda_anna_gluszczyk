/*
 * List.hpp
 *
 *  Created on: 01.04.2017
 *      Author: RENT
 */

#ifndef LIST_HPP_
#define LIST_HPP_


#include <iostream>

class Lista

{

private:

    class Wezel

    {

    public:

        int mWartosc;

        Wezel * nast;

        Wezel(int wartosc)

        {

            mWartosc = wartosc;

            nast = 0;

        }

    };

    Wezel* head; //wksaznik na pierwszy element

    Wezel* tail; //wskaznik na ostatni element

public:

    Lista()

    {

        head = 0;

        tail = 0;

    }

    void wypisz()

    {

        Wezel* tmp = 0; //tmo będzie wskaznikiem na aktualnie przegladany wezel

        tmp = head; //ustaw aktualny przegladany wezel na poczatek listy

        std::cout << "{ ";

        while (tmp != 0) //test czy nie jest to ostatni element

        {

            std::cout << "[" << tmp->mWartosc << "] ";

            tmp = tmp->nast; //pobierz nastepny element z listy

        }

        std::cout << " }" << std::endl;

    }

    void dodajPoczatek(int wartosc)

    {

        if (head != 0) //sprawdz czy nasza lista jest pusta

        { //jak nie jest pusta

            Wezel* nowyWezel = new Wezel(wartosc); //stwórz nowy węzel a następnie przypisz go do tymczasowego wskaznika

            nowyWezel->nast = head; //Niech nowy wezel wskazuje na to na co wskazuje czoło listy

            head = nowyWezel; //Niech czoło listy wskazuje teraz na nasz nowy Wezel

        }

        else

        { //jak jest pusta

            head = new Wezel(wartosc); //stwórz nowy Wezel, nastepnie niech czoło na niego wskazuje

            tail = head; //ustaw ogon na nowy element (czyli na ten ktory wskazuje head)_

        }

    }

    void dodajKoniec(int wartosc)

    {if (head != 0) //sprawdz czy nasza lista jest pusta

    { //jak nie jest pusta

        Wezel* nowyWezel = new Wezel(wartosc); //stwórz nowy węzel a następnie przypisz go do tymczasowego wskaznika

        tail->nast = nowyWezel; //Niech nowy wezel wskazuje na to na co wskazuje czoło listy

        tail = nowyWezel; //Niech czoło listy wskazuje teraz na nasz nowy Wezel

    }

    else

    { //jak jest pusta

        head = new Wezel(wartosc); //stwórz nowy Wezel, nastepnie niech czoło na niego wskazuje

        tail = head; //ustaw ogon na nowy element (czyli na ten ktory wskazuje head)_

    }


    }

	int pobierz(int indeks) //funkcja zwracajaca wartosc elemetnu pod pozycja n
	{
		Wezel*tmp = head;
		int i = 0;
		while (tmp != 0)
		{
			i++;
			if (i == indeks)
			{
				return tmp->mWartosc;
			}
			else
			{
				tmp = tmp->nast;
			}
			}
		return 0;
	}
	int znajdzPozycje(int wartosc) //znajdz indeks elementu o podanej wartosci
	{

		Wezel* tmp = head;
		int pozycja = 0; //0 jesli element nie zostal znaleziony
		while (tmp != 0)  //dopoki nie odwiedzilismy ostatniego wezla
		{
			pozycja++;
			if (wartosc == tmp->mWartosc)
			{
				return pozycja;
			}
			else
			{
				tmp = tmp->nast;
			}
		}

		return 0;
	}

	bool czyPusta() // funkcja zwracajaca true jesli lista jest pusta
	{
		if (head != 0)
		{

			return false;
		}
		else
		{
			return true;
		}
	}

	void wyczysc() //funkcja usuwajace wszystkie Wezly z listy
	{

		Wezel* tmp = 0; //tmo będzie wskaznikiem na aktualnie przegladany wezel
		tmp = head; //ustaw aktualny przegladany wezel na poczatek listy
		while (tmp != 0) //test czy nie jest to ostatni element

		{
			head = tmp->nast;
			delete tmp;
			tmp = head; //pobierz nastepny element z listy
		}

		head = 0;
		tail = 0;

	}

    void usun (int indeks) //funkcja ususwajace element na podanej pozycji od head

	{
		Wezel*tmp = head;
		Wezel* pop = 0;
		int i = 0;
		while (tmp != 0)
		{
			i++;
			if (i == indeks)
			{
				pop->nast = tmp->nast;
				delete tmp;
			}
			else
			{
				pop = tmp;
				tmp = tmp->nast;
			}
		}
	}
};



#endif /* LIST_HPP_ */
