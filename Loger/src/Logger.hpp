/*
 * Logger.hpp
 *
 *  Created on: 10.04.2017
 *      Author: RENT
 */

#ifndef LOGGER_HPP_
#define LOGGER_HPP_

#include <iostream>
#include <fstream>


class Logger
{
	std::ofstream file;
	int level;


public:
Logger (std::string filename,int log_level );

~Logger ();
void Log (std::string message, int log_level);

};





#endif /* LOGGER_HPP_ */
