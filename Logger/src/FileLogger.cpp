/*
 * FileLogger.cpp

 *
 *  Created on: 13.05.2017
 *      Author: RENT
 */
#include "FileLogger.hpp"
using namespace std;

FileLogger *FileLogger::instance =0;

FileLogger::FileLogger()
{
	mFile.open("log.txt");
}

FileLogger*FileLogger::getInstance()
{
	if (!instance)
    {
	instance = new FileLogger;
    }

return instance;
}

void FileLogger::log (string dataToLog)
{
	mFile<<dataToLog<<endl;
}
