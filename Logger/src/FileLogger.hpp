/*
 * FileLogger.hpp
 *
 *  Created on: 13.05.2017
 *      Author: RENT
 */

#ifndef FILELOGGER_HPP_
#define FILELOGGER_HPP_
#include <iostream>
#include<fstream>

using namespace std;

class FileLogger
{
private:
	ofstream mFile;
	static FileLogger *instance;
	FileLogger();

public:
	static FileLogger *getInstance();
	void log (string dataToLog);
};



#endif /* FILELOGGER_HPP_ */
