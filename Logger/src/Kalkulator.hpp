/*
 * Kalkulator.hpp
 *
 *  Created on: 13.05.2017
 *      Author: RENT
 */

#ifndef KALKULATOR_HPP_
#define KALKULATOR_HPP_
#include "FileLogger.hpp"

class Kalkulator
{
public:
	int podziel(int a, int b)
	{

		FileLogger::getInstance()->log("kalkulator,podziel");
		if (b == 0)
		{
			FileLogger::getInstance()->log("nie dzieli sie przez zero");
			return 0;
		}
		else
		{
			return a / b;
		}
	}

};


#endif /* KALKULATOR_HPP_ */
