/*
 * Matma.hpp
 *
 *  Created on: 24.04.2017
 *      Author: RENT
 */

#ifndef MATMA_HPP_
#define MATMA_HPP_

#include <iostream>
using namespace std;

class Matematyka
{
public:
Matematyka(){}
~Matematyka (){}

int silnia (unsigned int x)
	{

			if (x == 0)
			{
				return 1;
			}
			else
			{
				return x * silnia(x - 1);
			}

	}

int fib (unsigned int x)
	{
		if (x == 0)
		{
			return 0;
		}
		else if (x == 1)
		{
			return 1;
		}
		else
		{
			return fib(x- 1) + fib(x - 2);
		}
	}

int NWD(int x, int y)
	{

		if (x == 0)
		{
			return y;
		}
		else
		{
			int r;
			r = y % x;
			return NWD(r, x);
		}

	}


};



#endif /* MATMA_HPP_ */
