/*
 * MatmaTest.cpp
 *
 *  Created on: 24.04.2017
 *      Author: RENT
 */

#include "gtest/gtest.h"
#include "Matma.hpp"

TEST(Matmatest, Silnia)
{
	Matematyka a;
	EXPECT_EQ(24, a.silnia(4));
	EXPECT_EQ(1, a.silnia(0));
}

TEST(Matmatest, Fibonacci)
{
	Matematyka a;
	EXPECT_EQ(0, a.fib(0));
	EXPECT_EQ(1, a.fib(1));
	EXPECT_EQ(3, a.fib(4));
}

TEST(Matmatest, NWD)
{
	Matematyka a;
	EXPECT_EQ(4, a.NWD(4,8));
	EXPECT_EQ(8, a.NWD(0,8));
}
int main (int argc, char **argv)
{
	::testing::InitGoogleTest(&argc, argv);

	return RUN_ALL_TESTS();
}
