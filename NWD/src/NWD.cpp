//============================================================================
// Name        : NWD.cpp
// Author      : 
// Version     :
// Copyright   : Your copyright notice
// Description : Hello World in C++, Ansi-style
//============================================================================

#include <iostream>
using namespace std;

int NWD(int m, int n)
{

	if (m == 0)
	{
		return n;
	}
	else
	{
		int r;
		r = n % m;
		return NWD(r, m);
	}

}

int main()
{
	cout << "NWD = " << NWD(8,12) << endl;

	return 0;
}
