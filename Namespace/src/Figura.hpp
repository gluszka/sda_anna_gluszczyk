/*
 * Figura.hpp
 *
 *  Created on: 26.04.2017
 *      Author: RENT
 */

#ifndef FIGURA_HPP_
#define FIGURA_HPP_

class Figura
 {
   public:
     virtual void wypisz()=0;
     virtual~Figura(){}
 };



#endif /* FIGURA_HPP_ */
