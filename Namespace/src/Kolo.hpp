/*
 * Kolo.hpp
 *
 *  Created on: 26.04.2017
 *      Author: RENT
 */

#ifndef KOLO_HPP_
#define KOLO_HPP_
#include "Figura.hpp"

class Kolo :public Figura
{
	int mR;
	Kolor::Kolor mKolor;
public:

	Kolo (int r,Kolor::Kolor kolor)
	:mR(r)
	,mKolor(kolor)
	{}

void wypisz ()
{
	std::cout<<"promien "<<mR<<"kolor "<<Kolor::convertToString(mKolor)<<std::endl;
}

};




#endif /* KOLO_HPP_ */
