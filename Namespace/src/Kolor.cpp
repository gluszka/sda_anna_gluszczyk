#include "Kolor.hpp"
#include <iostream>

std::string Kolor::convertToString (Kolor kolor)
{
	std::string ret = "";
	switch (kolor)
	{
	case bialy:
		ret ="bialy";
		break;
	case czarny:
		ret ="czarny";
		break;
	case czerwony:
		ret ="czerwony";
		break;
	default:
		ret = "nieznany";
		break;
	}
	return ret;
}
