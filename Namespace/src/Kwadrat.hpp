/*
 * Kwadrat.hpp
 *
 *  Created on: 26.04.2017
 *      Author: RENT
 */

#ifndef KWADRAT_HPP_
#define KWADRAT_HPP_
#include "Figura.hpp"

class Kwadrat :public Figura
{
   int mX;
   Kolor::Kolor mKolor;

public:
	Kwadrat (int x,Kolor::Kolor kolor)
		:mX(x)
		,mKolor(kolor)
		{}

	void wypisz()
	{
		std::cout << "promien " << mX << "kolor " <<Kolor::convertToString(mKolor) << std::endl;
	}
};



#endif /* KWADRAT_HPP_ */
