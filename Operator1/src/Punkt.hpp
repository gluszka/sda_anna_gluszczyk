/*
 * Punkt.hpp
 *
 *  Created on: 06.05.2017
 *      Author: RENT
 */

#ifndef PUNKT_HPP_
#define PUNKT_HPP_

class Punkt
{
public:
	int getX() const
	{
		return mX;
	}

	void setX(int x)
	{
		mX = x;
	}

	int getY() const
	{
		return mY;
	}

	void setY(int y)
	{
		mY = y;
	}

private:
	int mX;
	int mY;

public:
	Punkt operator *(const int multi) const
	{
		Punkt wynik(0,0);
		wynik.setX(mX * multi);
		wynik.setY(mY * multi);
		return wynik;
	}
bool operator==(const Punkt &pkt)const
	{
		if (pkt.getX() == this->getX() && pkt.getY() == this->getY())
		{
			return true;
		}
		else
		{
			return false;
		}
	}
	Punkt(int x, int y)
			:mX(x), mY(y)
	{
	}

};

Punkt operator* (const int multi,const Punkt &pkt)
{
	return pkt * multi;

}


#endif /* PUNKT_HPP_ */
