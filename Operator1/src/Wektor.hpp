/*
 * Wektor.hpp
 *
 *  Created on: 06.05.2017
 *      Author: RENT
 */

#ifndef WEKTOR_HPP_
#define WEKTOR_HPP_
#include "Punkt.hpp"

class Wektor
{
private:
	Punkt mP1;
	Punkt mP2;

public:

	void operator!()
	{
		Punkt tmp = mP1;
		mP1 =mP2;
		mP2 =tmp;
	}
bool operator ==(const Wektor & w2)
	{
		if (w2.getP1() == this->getP1() && w2.getP2() == this->getP2())
		{
			return true;
		}
		else
		{
			return false;
		}

	}


	Wektor(Punkt p1, Punkt p2) :
			mP1(p1), mP2(p2)
	{
	}

	void wypisz()
	{
		std::cout << "P1.x= " << mP1.getX()<<" " << "P1.y= " << mP1.getY()<<" "
				<< "P2.x= " << mP2.getX() <<"  "<< "P2.y= " << mP2.getY()
				<< std::endl;
	}

const Punkt& getP1() const
{
	return mP1;
}

void setP1(const Punkt& p1)
{
	mP1 = p1;
}

const Punkt& getP2() const
{
	return mP2;
}

void setP2(const Punkt& p2)
{
	mP2 = p2;
}
};



#endif /* WEKTOR_HPP_ */
