/*
 * Punkt.hpp
 *
 *  Created on: 06.05.2017
 *      Author: RENT
 */
#include <iostream>

#ifndef PUNKT_HPP_
#define PUNKT_HPP_

class Punkt
{
private:
	float mX;
	float mY;

public:
	Punkt(float x, float y)
	:mX(x)
	,mY(y)
		{
		}
Punkt()
:mX(0)
,mY(0)
		{
		}

Punkt(const Punkt& pkt)
	:mX(pkt.getX())
	,mY(pkt.getY())
		{
		}

	float getX() const
	{
		return mX;
	}

	void setX(float x)
	{
		mX = x;
	}

	float getY() const
	{
		return mY;
	}

	void setY(float y)
	{
		mY = y;
	}

	Punkt operator *(const float multi) const
		{
			Punkt wynik(0,0);
			wynik.setX(mX * multi);
			wynik.setY(mY * multi);
			return wynik;
		}

	Punkt operator + (const Punkt & p)const
		{
			Punkt pkt(0.0,0.0);
			pkt.setX(mX + p.getX());
			pkt.setY(mY + p.getY());
			return pkt;
		}

	Punkt operator +(const float add) const
			{
				Punkt wynik(0,0);
				wynik.setX(mX + add);
				wynik.setY(mY + add);
				return wynik;
			}
Punkt & operator++()
		{
	this->setX(mX+1);
	this ->setY(mY+1);
	return *this;
		}
	Punkt operator++(int)
	{
		Punkt tmp(*this);
		this->operator++();
		return tmp;
	}
	void wypisz()
		{
			std::cout << "x= " <<mX<<" " << "y=" << mY<< std::endl;
		}
};

Punkt operator +(const float k, const Punkt&pkt)
{
	return pkt + k;
}

#endif /* PUNKT_HPP_ */
