/*
 * Zbior.hpp
 *
 *  Created on: 06.05.2017
 *      Author: RENT
 */

#ifndef ZBIOR_HPP_
#define ZBIOR_HPP_
#include "Punkt.hpp"
#include <iostream>

class Zbior
{
private:
	Punkt** mPtr;
	size_t mSize;

void resize ()
{
		if (mSize == 0)

		{
			mPtr = new Punkt*[++mSize];
		}
		else
		{
			Punkt**tmp = new Punkt*[++mSize];

			for (size_t i = 0; i < mSize - 1; i++)
			{
				tmp[i] = mPtr[i];
			}
			delete[]mPtr;
			mPtr = tmp;
		}
}
public:
Zbior ()
:mPtr(0)
,mSize(0)
{
}

~Zbior()
	{
		if (mSize != 0)
			for (size_t i = 0; i < mSize; i++)
			{
				delete mPtr[i];
			}
		{
			delete[] mPtr;
			mSize = 0;
		}
	}
	void wypisz()
	{
		for (size_t i = 0; i < mSize; i++)
		{
			std::cout << "p" << "{";
			mPtr[i]->wypisz();
			std::cout << "}";
		}
	}

void operator + (const Punkt&pkt)
{
resize();
mPtr[mSize-1]= new Punkt(pkt);
}

Punkt operator[](size_t idx) const
{
	if (idx < mSize)
		{
		return Punkt(*mPtr[idx]);
		}
	else
	{
	return Punkt(0,0);
	}
}

void operator + (const Zbior &zbior)
	{
		for (size_t i = 0; i < zbior.getSize(); i++)
		{
			*this + zbior[i];
		}
	}

operator bool()
		{
	return (this-> getSize()!=0);
		}
	size_t getSize() const
	{
		return mSize;
	}

	void setSize(size_t size)
	{
		mSize = size;
	}
};



#endif /* ZBIOR_HPP_ */
