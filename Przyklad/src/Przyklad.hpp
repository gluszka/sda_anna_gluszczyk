/*
 * Przyklad.hpp
 *
 *  Created on: 26.04.2017
 *      Author: RENT
 */

#ifndef PRZYKLAD_HPP_
#define PRZYKLAD_HPP_
#include <string>

extern int extInt;
extern std::string extString;
extern float extFloat;

void zmienString (std::string tekst);
void zmienFloat (float x);

#endif /* PRZYKLAD_HPP_ */
