/*
 * KolorowyPunkt.cpp
 *
 *  Created on: Mar 28, 2017
 *      Author: orlik
 */

#include "KolorowyPunkt.hpp"
#include <iostream>
using namespace std;


KolorowyPunkt::KolorowyPunkt(int x, int y, Kolor kolor)
: Punkt(x,y)
, mKolor(kolor)
{
}

void KolorowyPunkt::wypisz()
{
		Punkt::wypisz();
		switch (mKolor)
		{
		case red:
			cout << "czerwony" << endl;
			break;
		case green:
			cout << "zielony" << endl;
			break;
		case blue:
			cout << "niebieski" << endl;
			break;
		default:
		cout<<"nie ma takiego koloru"<<endl;
		}
}




