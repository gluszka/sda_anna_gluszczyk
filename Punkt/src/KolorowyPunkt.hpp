/*
 * KolorowyPunkt.hpp
 *
 *  Created on: Mar 28, 2017
 *      Author: orlik
 */

#ifndef KOLOROWYPUNKT_HPP_
#define KOLOROWYPUNKT_HPP_

#include "Punkt.hpp"
#include <string>

class KolorowyPunkt : public Punkt
{
public:
	enum Kolor //deklaracja typu wyliczeniowego, który znajduje sie w naszej klasie KolorowyPunkt
	{
		red,
		green,
		blue
	};

	KolorowyPunkt(int x, int y, Kolor kolor);
	void wypisz();



protected:
	Kolor mKolor;
};

#endif /* KOLOROWYPUNKT_HPP_ */
