/*
 * Punkt.cpp
 *
 *  Created on: Mar 28, 2017
 *      Author: orlik
 */

#include "Punkt.hpp"
#include <iostream>
#include <cmath>

Punkt::Punkt()
: mX(0)
, mY(0)
{
}

Punkt::Punkt(int x, int y)
: mX(x)
, mY(y)
{
}

int Punkt::getX() const
{
	return mX;
}

void Punkt::setX(int x)
{
	mX = x;
}

int Punkt::getY() const
{
	return mY;
}

void Punkt::setY(int y)
{
	mY = y;
}

void Punkt::wypisz()
{
	std::cout<<"["<<mX<<","<<mY<<"]\n";
}

void Punkt::przesun(Punkt p)
{
	przesun(p.getX(), p.getY());
}

void Punkt::przesun(int x, int y)
{
	przesunX(x);
	przesunY(y);
}

void Punkt::przesunX(int x)
{
	setX(mX+x);
}

void Punkt::przesunY(int y)
{
	setY(mY+y);
}

float Punkt::obliczOdleglosc(Punkt p)
{
	return obliczOdleglosc(p.getX(), p.getY());
}

float Punkt::obliczOdleglosc(int x, int y)
{
	int dX = mX-x;
	int dY = mY-y;
	return sqrt(dX*dX + dY*dY);
}
