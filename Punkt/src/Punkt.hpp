/*
 * Punkt.hpp
 *
 *  Created on: Mar 28, 2017
 *      Author: orlik
 */

#ifndef PUNKT_HPP_
#define PUNKT_HPP_

class Punkt
{
public:

	Punkt();
	Punkt(int x, int y);

	int getX() const;
	void setX(int x);
	int getY() const;
	void setY(int y);

	void wypisz();
	void przesun(Punkt p);
	void przesun(int x, int y);
	void przesunX(int x);
	void przesunY(int y);
	float obliczOdleglosc(Punkt p);
	float obliczOdleglosc(int x, int y);

protected:
	int mX;
	int mY;
};

#endif /* PUNKT_HPP_ */
