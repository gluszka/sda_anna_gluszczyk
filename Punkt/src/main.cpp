/*
 * main.cpp
 *
 *  Created on: Mar 28, 2017
 *      Author: orlik
 */
#include "Punkt.hpp"
#include "KolorowyPunkt.hpp"
#include <iostream>

int main()
{
	Punkt p1(-10,10);
	Punkt p2(0,0);

	p1.wypisz();
	p2.wypisz();

	p1.przesun(5,5);
	p1.wypisz();

	std::cout<<"Odleglosc= "<<p1.obliczOdleglosc(p2)<<std::endl;

	p2.przesun(p1);
	p2.wypisz();
	p2.przesun(-1,-1);
	p2.wypisz();

	Punkt p7;
	p7.setX(p1.getX());
	p7.setY(p2.getY());
	p7.wypisz();

	KolorowyPunkt zielony(3,3, KolorowyPunkt::green);
	KolorowyPunkt p131(13, 13, KolorowyPunkt::blue);

	zielony.wypisz();
	p131.wypisz();


	return 0;
}
