//============================================================================
// Name        : RPNStos.cpp
// Author      : 
// Version     :
// Copyright   : Your copyright notice
// Description : Hello World in C++, Ansi-style
//============================================================================

#include <iostream>
using namespace std;
#include <stack>

int wylicz(const std::string wyrazenie)
{
std::stack<int> stos;

for (unsigned i = 0; i < wyrazenie.length(); ++i)
	{
		char znak = wyrazenie[i];
		int liczba = znak - '0';
		if (znak == ' ')
		{
			continue;
		}
		if (znak >= '0' && znak <= '9')
		{
			stos.push(liczba);
		}
		else
		{
			int wynik;
			int a = stos.top();
			stos.pop();
			int b = stos.top();
			stos.pop();
			switch (znak)
			{
			case '+':
				wynik = b + a;
				stos.push(wynik);
				break;
			case '-':
				wynik = b - a;
				stos.push(wynik);
				break;
			case '*':
				wynik = b * a;
				stos.push(wynik);
				break;
			case '/':
				wynik = b / a;
				stos.push(wynik);
				break;
			}
		}
	}
	return stos.top();
}

void sprawdz(const std::string rpn, int oczekiwanyWynik)
{
	int wynik = wylicz(rpn);
	if (wynik == oczekiwanyWynik)
	{
		std::cout << "OK ";
	}
	else
	{
		std::cout << "Error  ";
	}
	std::cout << rpn << " => " << wynik << std::endl;
}

const std::string RPN[] = { "3 2 + 4 -", "1 1 1 1 + + +", "5 1 2 + 4 * + 3 -" };
const int wyniki[] = { 1, 4, 14};


int main()
{
	for (int i = 0; i < 3; ++i)
	{
		sprawdz(RPN[i], wyniki[i]);
	}
	return 0;
}
