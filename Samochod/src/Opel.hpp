/*
 * Opel.hpp
 *
 *  Created on: 27.03.2017
 *      Author: RENT
 */

#ifndef OPEL_HPP_
#define OPEL_HPP_
#include"Samochod.hpp"

class Opel : public Samochod
{
	private:
	std::string mKolor;
	std::string mMarka;
	float mPojemnosc;

public:
	Opel (std::string kolor,std::string marka,float pojemnosc);

	~ Opel()
	{std::cout<<"~Opel"<<std::endl;
	}

		void wypiszKolor ();
		void wypiszMarka ();
		void wypiszPojemnosc ();

};




#endif /* OPEL_HPP_ */
