/*
 * Samochod.hpp
 *
 *  Created on: 27.03.2017
 *      Author: RENT
 */

#ifndef SAMOCHOD_HPP_
#define SAMOCHOD_HPP_

#include <iostream>
#include <string>

class Samochod {protected:

public:
	virtual void wypiszKolor () =0;
	virtual void wypiszMarka ()=0;
	virtual void wypiszPojemnosc ()=0;


	virtual ~Samochod()
	{
		std::cout << "~Samochod" << std::endl;
	}


};



#endif /* SAMOCHOD_HPP_ */
