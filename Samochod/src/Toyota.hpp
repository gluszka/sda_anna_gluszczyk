/*
 * Toyota.hpp
 *
 *  Created on: 27.03.2017
 *      Author: RENT
 */

#ifndef TOYOTA_HPP_
#define TOYOTA_HPP_
#include "Samochod.hpp"

class Toyota: public Samochod

{
	private:
	std::string mKolor;
	std::string mMarka;
	float mPojemnosc;

public:
	Toyota (std::string kolor,std::string marka,float pojemnosc);

	~ Toyota ()
		{std::cout<<"~Toyota"<<std::endl;
		}
	void wypiszKolor ();
	void wypiszMarka ();
	void wypiszPojemnosc ();

};



#endif /* TOYOTA_HPP_ */
