//============================================================================
// Name        : Shape.cpp
// Author      : 
// Version     :
// Copyright   : Your copyright notice
// Description : Hello World in C++, Ansi-style
//============================================================================

#include <iostream>
using namespace std;

class Shape
{
public:
	Shape()
	{
	}
	virtual float pole()= 0;
	virtual void nazwa()=0;

	virtual ~ Shape()
	{
	}
};

class Circle :public Shape

{protected:
	int mR;
	float pi = 3.14159;

public:
	Circle(int r)
	:mR(r)
	{
	}

	float pole()
	{
		return pi * mR * mR;
	}

	void nazwa()
	{cout<<"kolo"<<endl;
	}

	~ Circle()
		{
		}
	};

class Cylinder :public Circle
{
int mR;
float pi = 3.14159;
int mH;

public:

Cylinder ( int r,int h)
:Circle(r)
,mR(r)
,mH(h)
	{
	}

float pole()
	{
		return (2*(Circle::pole())) + (2* pi* mR* mH);
	}
void nazwa()
	{cout<<"cylinder"<<endl;
	}

~ Cylinder()
	{
	}
};


int main() {
Circle circle (2);
cout<<circle.pole()<<endl;
Cylinder cylinder (2,3);
cout<<cylinder.pole()<<endl;
	return 0;
}
