//============================================================================
// Name        : String.cpp
// Author      : 
// Version     :
// Copyright   : Your copyright notice
// Description : Hello World in C++, Ansi-style
//============================================================================
#include "String.hpp"
#include "gtest/gtest.h"

TEST (String, Getter)
{
String a ("abc");
EXPECT_EQ ("abc", a.getString());
}

TEST (String, Suma)
{
	String b ("abc");
	EXPECT_EQ (294 , b.suma());
}

int main(int argc, char **argv)
		{
			::testing::InitGoogleTest(&argc, argv);

			return RUN_ALL_TESTS();
		}
