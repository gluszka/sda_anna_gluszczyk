/*
 * Pracownik.cpp
 *
 *  Created on: 25.03.2017
 *      Author: RENT
 */
#include <iostream>
using namespace std;
#include "Pracownik.hpp"
#include "Osoba.hpp"

Pracownik::Pracownik (float pensja, std::string zawod, long nrPesel)
:Osoba (nrPesel)
,mPensja(pensja)
 ,mZawod(zawod)
{

}

Pracownik::Pracownik (float pensja, std::string zawod)
: mPensja(pensja)
 ,mZawod(zawod)
{

}

void Pracownik::pracuj ()
{
	cout<<"pracuje"<<endl;
}

