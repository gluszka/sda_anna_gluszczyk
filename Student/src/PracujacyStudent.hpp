/*
 * PracujacyStudent.hpp
 *
 *  Created on: 25.03.2017
 *      Author: RENT
 */

#ifndef PRACUJACYSTUDENT_HPP_
#define PRACUJACYSTUDENT_HPP_
#include "Student.hpp"
#include "Pracownik.hpp"

class PracujacyStudent: public Student, public Pracownik
{
public:
	PracujacyStudent (int nrIndeksu, std::string kierunek, float pensja, std::string zawod, long nrPesel);

void uczSie ();
void pracuj ();
};

#endif /* PRACUJACYSTUDENT_HPP_ */
