/*
 * Student.hpp
 *
 *  Created on: 25.03.2017
 *      Author: RENT
 */

#ifndef STUDENT_HPP_
#define STUDENT_HPP_
#include <string>
#include "Osoba.hpp"

class Student: public virtual Osoba
{
private:
	int mNrIndeksu;
	std::string mKierunek;


public:
	Student (int nrIndeksu, std::string kierunek, long nrPesel);
	Student (int nrIndeksu, std::string kierunek);

	void podajNumer ();
	void virtual uczSie();

};



#endif /* STUDENT_HPP_ */
