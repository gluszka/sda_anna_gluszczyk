/*
 * CezarTekst.hpp
 *
 *  Created on: 30.03.2017
 *      Author: RENT
 */

#ifndef CEZARTEKST_HPP_
#define CEZARTEKST_HPP_
#include "SzyfrowanyTekst.hpp"

class CezarTekst :public SzyfrowanyTekst
{
	public:
	CezarTekst();

	~CezarTekst();
	int przesuniecie;
   void szyfruj ();
   void wypisz ();

	int getPrzesuniecie() const
	{
		return przesuniecie;
	}

	void setPrzesuniecie(int przesuniecie)
	{
		this->przesuniecie = przesuniecie;
	}
};



#endif /* CEZARTEKST_HPP_ */
