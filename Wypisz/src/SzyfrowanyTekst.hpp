/*
 * Abstrakcyjna.hpp
 *
 *  Created on: 30.03.2017
 *      Author: RENT
 */

#ifndef SZYFROWANYTEKST_HPP_
#define SZYFROWANYTEKST_HPP_
#include "Wypisywalny.hpp"
#include <iostream>

class SzyfrowanyTekst :public Wypisywalny
{public:
	virtual void szyfruj ()= 0;
	virtual void wypisz ()=0;
    bool mZaszyfrowane;


    SzyfrowanyTekst ()
	:mZaszyfrowane (false)
	{
		std::cout<<"SzyfrowanyTekst konstruktor"<<std::endl;
	}

virtual ~SzyfrowanyTekst ()
	{std::cout<<"SzyfrowanyTekst destruktor"<<std::endl;
	}

	bool czyZaszyfrowane()
	{
	return mZaszyfrowane;
	}
};



#endif /* SZYFROWANYTEKST_HPP_ */
