/*
 * Wypisz.cpp
 *
 *  Created on: 30.03.2017
 *      Author: RENT
 */
#include "Wypisywalny.hpp"

#include <iostream>

Wypisywalny::Wypisywalny()
	{
	std::cout<<"Tworze Wypisywalny"<<std::endl;
	}

Wypisywalny::Wypisywalny(std::string lancuch) :
			mLancuch(lancuch)
	{ std::cout<<"Tworze "<<mLancuch<<std::endl;
	}

Wypisywalny::~Wypisywalny()
{std::cout<<"Niszcze "<<mLancuch<<std::endl;
}

const std::string& Wypisywalny::getLancuch() const
	{
		return mLancuch;
	}

void Wypisywalny::setLancuch(const std::string& lancuch)
	{
		mLancuch = lancuch;
	}
void Wypisywalny::wypisz()
{
	std::cout << "Tekst: " << mLancuch << std::endl;
}
