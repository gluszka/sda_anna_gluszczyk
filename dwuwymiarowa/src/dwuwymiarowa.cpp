//============================================================================
// Name        : dwuwymiarowa.cpp
// Author      : 
// Version     :
// Copyright   : Your copyright notice
// Description : Hello World in C++, Ansi-style
//============================================================================

#include <iostream>

void pokaz(float** m, int maxX, int maxY);

int main()
{
	const int stala[4][5] = { { 1, 2, 3, 4, 5 }, { 6, 7, 8, 9, 10 }, { 11, 12,
			13, 14, 15 }, { 16, 17, 18, 19, 20 }, };

	std::cout << stala[1][4] << std::endl;

	float** macierz = NULL;

	// stworzyć dynamicznie dwuwymiarową tablicę floatów
	macierz = new float*[4];
	macierz[0]=new float[5];
	macierz[1]=new float[5];
	macierz[2]=new float[5];
	macierz[3]=new float[5];

	for (int y=0; y<4;y++)
	{
for(int x=0;x<5;x++){
	macierz[y][x]=stala [y][x]*1.5;
}
	}

	// przepisać wartośći ze stałej tablicy tablic, mnożąc każdą wartość przez 1.5f

	// pokazać przemnożoną tablicę za pomocą funkcji pokaz()
	pokaz(macierz,5,4);

	// zwolnić tablicę dwuwymiarową
	for (int i=0; i<4; i++)
	{
delete [] macierz [i];
	}
delete [] macierz;

	return 0;
}

void pokaz(float** m, int maxX, int maxY)
{
	for (int y = 0; y < maxY; ++y)
	{
		for (int x = 0; x < maxX; ++x)
		{
			std::cout << m[y][x] << ' ';
		}
		std::cout << std::endl;
	}

}
