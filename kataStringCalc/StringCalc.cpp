/*
 * kataStringCalc.cpp
 *
 *  Created on: 20.04.2017
 *      Author: RENT
 */
#include "StringCalc.hpp"
#include <cstdlib>

int StringCalc::Add(std::string numbers)
{
	if( numbers.size()== 0)
	{
		return 0;
	}
	else if (numbers.find(",")== std::string::npos)
	{
		return std::atoi(numbers.c_str());
	}
	else
	{
		int comaPosition = numbers.find(",");
		std::string numberOne = numbers.substr(0,comaPosition);
		std::string numberTwo = numbers.substr(comaPosition+1, std::string::npos);

		return  std::atoi(numberOne.c_str()) + std::atoi(numberTwo.c_str());
	}
	return -1;
}
