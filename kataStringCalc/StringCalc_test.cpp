/*
 * StringCalc_test.cpp
 *
 *  Created on: 20.04.2017
 *      Author: RENT
 */
#include "gtest/gtest.h"
#include "StringCalc.hpp"


TEST(StringCalculator, EmptyString)
{
StringCalc c;
EXPECT_EQ(0, c.Add(""));
}

TEST(StringCalculator, OneNumber)
{
StringCalc c;
EXPECT_EQ(1, c.Add("1"));
}

TEST (StringCalculator, TwoNumbers)
{
StringCalc c;
EXPECT_EQ(3, c.Add("1,2"));
EXPECT_EQ(33, c.Add("12,21"));
EXPECT_EQ(153, c.Add("141,12"));
}

int main(int argc, char **argv) {
      ::testing::InitGoogleTest(&argc, argv);

      return RUN_ALL_TESTS();
}
