//============================================================================
// Name        : licznik.cpp
// Author      : 
// Version     :
// Copyright   : Your copyright notice
// Description : Hello World in C++, Ansi-style
//============================================================================

#include <iostream>
using namespace std;

class Zlicznikiem
{
//private:

public:
	int licznik;

	Zlicznikiem() // :licznik(1)
	{
		licznik = 1;
	}

	Zlicznikiem(const Zlicznikiem& zrodlo)
	{
		licznik = zrodlo.licznik + 1;
	}

	~Zlicznikiem()
	{
		std::cout << licznik << std::endl;
	}
};

void funkcja(Zlicznikiem c)
{
	c.licznik += 100;
//	Zlicznikiem d(c);
}

void funkcja2(Zlicznikiem c)
{
	c.licznik += 500;
//	Zlicznikiem d(c);
}

int main()
{
	Zlicznikiem c;
	funkcja(c);		   // tu jest wywołany kons. kopiujący
	Zlicznikiem d = c; // tu jest wywołany kons. kopiujący
	Zlicznikiem e;
	funkcja2(e);
	return 0;
}
