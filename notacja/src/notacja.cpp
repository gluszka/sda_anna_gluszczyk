#include <iostream>

#include <string>

const std::string RPN[] =

{ "3 2 + 4 -", "1 1 1 1 + + +", "5 1 2 + 4 * + 3 -",

"1 1 + 2 + 3 1 1 + 7 - 9 / * -" };

const int wyniki[] =

{ 1, 4, 14, 4 };

int wylicz(const std::string wyrazenie)

{
	int stos[20];
	int wierzcholek = 0;

	for (unsigned i = 0; i < wyrazenie.length(); ++i)

	{
		char znak = wyrazenie[i];
		if (znak == ' ')
		{
			continue;
		}
		if (znak >= '0' && znak <= '9')
		{
			stos[wierzcholek] = znak - '0';
			wierzcholek += 1;
		}
		else
			{
			int plytsza,glebsza,wynik;
			switch (znak)
			{
			case '+':
				plytsza = stos[wierzcholek - 1];
				wierzcholek -= 1;
				glebsza = stos[wierzcholek - 1];
				wierzcholek -= 1;
				wynik = glebsza + plytsza;
				stos[wierzcholek] = wynik;
				wierzcholek += 1;
				break;
			case '-':
				stos[wierzcholek - 2] = stos[wierzcholek - 2]
						- stos[wierzcholek - 1];
				wierzcholek -= 1;
				break;
			case '*':
				stos[wierzcholek - 2] = stos[wierzcholek - 2]
						* stos[wierzcholek - 1];
				wierzcholek -= 1;
				break;
			case '/':
				stos[wierzcholek - 2] = stos[wierzcholek - 2]
						/ stos[wierzcholek - 1];
				wierzcholek -= 1;
				break;
			}
			}
	}
	return stos[0];

}

void sprawdz(const std::string rpn, int oczekiwanyWynik)

{

	int wynik = wylicz(rpn);

	if (wynik == oczekiwanyWynik)

	{

		std::cout << "Cacy ";

	}

	else

	{

		std::cout << "Be   ";

	}

	std::cout << rpn << " => " << wynik << std::endl;

}

int main(int argc, char* argv[])

{

	for (int i = 0; i < 4; ++i)

	{

		sprawdz(RPN[i], wyniki[i]);

	}

	return 0;

}
