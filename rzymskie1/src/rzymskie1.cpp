//============================================================================
// Name        : rzymskie1.cpp
// Author      : 
// Version     :
// Copyright   : Your copyright notice
// Description : Hello World in C++, Ansi-style
//============================================================================

#include <iostream>

int main()
{
	std::string rzym;
	std::cout << "podaj liczbę: " << std::flush;
	std::getline(std::cin, rzym);

	if (rzym.empty())
	{
		std::cout << "Koniec" << std::endl;
		return 0;
	}

	int wynik = 0;
	for (unsigned i = 0; i < rzym.length(); i++)
	{
		char znak = rzym[i];
		if (znak == 'I')
		{
			if (((i + 1) < rzym.length()) && (rzym[i + 1] == 'V')
					|| (rzym[i + 1] == 'X'))
			{
				wynik -= 1;
			}
			else
			{
				wynik += 1;
			}
		}
		else if (znak == 'V')
		{
			wynik = wynik + 5;
		}
		else if (znak == 'X')
		{
			if (i + 1 < rzym.length() && rzym[i + 1] == 'L'
					|| rzym[i + 1] == 'C')
			{
				wynik -= 10;
			}
			else
			{
				wynik = wynik + 10;
			}
		}
		else if (znak == 'L')
		{
			wynik = wynik + 50;
		}
		else if (znak == 'C')
		{
			if (i + 1 < rzym.length() && rzym[i + 1] == 'D'
					|| rzym[i + 1] == 'M')
			{
				wynik -= 100;
			}
			else
			{
				wynik = wynik + 100;
			}
		}
		else if (znak == 'D')
		{
			wynik = wynik + 500;
		}
		else if (znak == 'M')
		{
			wynik = wynik + 1000;
		}
	}
	std::cout << wynik << std::endl;
	return 0;
}
