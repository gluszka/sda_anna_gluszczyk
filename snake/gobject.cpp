#include <cstddef>
#include "gobject.hpp"

static int isEqual(const position *p1, const position *p2)
{
	return p1->x == p2->x && p1->y == p2->y;
}

void gobject_init(gobject *obj)
{
	obj->pos = NULL;
	obj->npos = 0;
	obj->symbol = '\0';
}

void gobject_resize(gobject *obj, size_t nmemb)
{
	position * newPos = new position[nmemb];
	for (int i = 0; i < obj->npos; ++i)

	{
		newPos[i].x = obj->pos[i].x;
		newPos[i].y = obj->pos[i].y;
	}

//	obj->pos = realloc(obj->pos, sizeof(*obj->pos) * nmemb);
	delete[] obj->pos;
	obj->pos = newPos;
	obj->npos = nmemb;
}

void gobject_destory(gobject *obj)
{
	delete[] obj->pos;
}

int gobject_collision(const gobject *obj1, const gobject *obj2)
{
	size_t i, j;

	for (i = 0; i < obj1->npos; ++i)
	{
		for (j = 0; j < obj2->npos; ++j)
		{
			if (isEqual(&obj1->pos[i], &obj2->pos[j]))
				return 1;
		}
	}

	return 0;
}

void gobject_move(gobject *obj, int x_pos, int y_pos)
{
	int x_delta, y_delta;
	size_t i;

	/* calculate delta from first position */
	x_delta = x_pos - obj->pos[0].x;
	y_delta = y_pos - obj->pos[0].y;

	/* move all position relative to the delta */
	for (i = 0; i < obj->npos; ++i)
	{
		obj->pos[i].x += x_delta;
		obj->pos[i].y += y_delta;
	}
}

void gobject_slither(gobject *obj, int x_delta, int y_delta)
{
	size_t i;

	/* move pos n to position n-1 to create slither movement */
	for (i = obj->npos - 1; i > 0; --i)
	{
		obj->pos[i].x = obj->pos[i - 1].x;
		obj->pos[i].y = obj->pos[i - 1].y;
	}

	/* move first element according to delta */
	obj->pos[0].x += x_delta;
	obj->pos[0].y += y_delta;
}

int gobject_occupying(const gobject *obj, const position *pos)
{
	size_t i;

	for (i = 0; i < obj->npos; ++i)
	{
		if (isEqual(&obj->pos[i], pos))
			return 1;
	}

	return 0;
}
