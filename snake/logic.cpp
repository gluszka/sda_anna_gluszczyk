#include <stdlib.h> /* rand() */

#include "board.hpp"
#include "globals.hpp"
#include "gobject.hpp"
#include "logic.hpp"
#include "score.hpp"

int snakehitWall(const gobject *snake, const board *board)
{
	return snake->pos[0].x < 0 || snake->pos[0].x >= board->length
			|| snake->pos[0].y < 0 || snake->pos[0].y >= board->width;
}

int snakeHitTail(const gobject *snake)
{
	size_t i;

	for (i = 1; i < snake->npos; ++i)
	{
		if (snake->pos[0].x == snake->pos[i].x
				&& snake->pos[0].y == snake->pos[i].y)
			return 1;
	}

	return 0;
}

void logic(board *board, gobject *snake, gobject *fruit, score *score,
		state *state)
{
	position pos;
pos.x = 0;
pos.y =0;
	/* fruit collision */
	if (gobject_collision(snake, fruit))
	{
		score_update(score, SCORE_FRUIT);
		gobject_move(fruit, rand() % board->length, rand() % board->width);
		gobject_resize(snake, snake->npos + 1);
	}

	switch (state->direction)
	{
	case UP:
		--pos.y;
		break;
	case LEFT:
		--pos.x;
		break;
	case DOWN:
		++pos.y;
		break;
	case RIGHT:
		++pos.x;
		break;
	default:
		break;
	}

	gobject_slither(snake, pos.x, pos.y);

	if (snakehitWall(snake, board) || snakeHitTail(snake))
	{
		state->game_state = CRASH;
	}
}
