#include "board.hpp"
#include "draw.hpp"
#include "globals.hpp"
#include "gobject.hpp"
#include "input.hpp"
#include "logic.hpp"
#include "score.hpp"

#include <cstdlib>
#include <iostream>
/*
 * Holds the game settings specified from the defaults and/or command-line
 * arguments
 */
struct settings
{
	int border_length;
	int border_width;
	int border_thickness;
	char border_symbol;
	char snake_symbol;
	char fruit_symbol;
};

/*
 * Sets the game to the initial state
 */
void __set_input_defaults(state *state)
{
	state->game_state = PAUSE;
	state->direction = STOP;
}

/*
 * Applies game settings to the board object
 */
void __set_board_defaults(board *board, settings *settings)
{
	board->length = settings->border_length;
	board->width = settings->border_width;
	board->border_thickness = settings->border_thickness;
	board->symbol = settings->border_symbol;
}

/*
 * Sets the snake object to its starting values
 */
void __set_snake_defaults(board *board, gobject *snake, settings *settings)
{
	gobject_resize(snake, 1);
	snake->pos[0].x = board->length / 2;
	snake->pos[0].y = board->width / 2;
	snake->symbol = settings->snake_symbol;
}

/*
 * Sets the fruit object to its starting values
 */
void __set_fruit_defaults(board *board, gobject *fruit, settings *settings)
{
	gobject_resize(fruit, 1);
	fruit->pos[0].x = rand() % board->length;
	fruit->pos[0].y = rand() % board->width;
	fruit->symbol = settings->fruit_symbol;
}

/*
 * Sets the score to its starting value
 */
void __set_score_default(score *score)
{
	score_clear(score);
}

/*
 * Sets the game into the initial state
 */
void reset(board *board, gobject *snake, gobject *fruit, score *score,
		state *state, settings *settings)
{
	__set_input_defaults(state);
	__set_board_defaults(board, settings);
	__set_snake_defaults(board, snake, settings);
	__set_fruit_defaults(board, fruit, settings);
	__set_score_default(score);
}

/*
 * Reads the input from stdin and updates the game state accordingly
 */
void input(state *state)
{
	char ch;
	ch = read_key();

	if (ch == GAME_QUIT)
	{
		state->game_state = QUIT;
		return;
	}

	/* Initial state */
	if (state->game_state == PAUSE && state->direction == STOP)
	{
		switch (ch)
		{
		case MOVE_UP:
			state->direction = UP;
			state->game_state = RUNNING;
			break;
		case MOVE_LEFT:
			state->direction = LEFT;
			state->game_state = RUNNING;
			break;
		case MOVE_DOWN:
			state->direction = DOWN;
			state->game_state = RUNNING;
			break;
		case MOVE_RIGHT:
			state->direction = RIGHT;
			state->game_state = RUNNING;
			break;
		}
	}
	/* Running state */
	else if (state->game_state == RUNNING)
	{
		switch (ch)
		{
		case MOVE_UP:
			state->direction = UP;
			break;
		case MOVE_LEFT:
			state->direction = LEFT;
			break;
		case MOVE_DOWN:
			state->direction = DOWN;
			break;
		case MOVE_RIGHT:
			state->direction = RIGHT;
			break;
		case GAME_PAUSE:
			state->game_state = PAUSE;
			break;
		}
	}
	/* Pause state */
	else if (state->game_state == PAUSE)
	{
		switch (ch)
		{
		case GAME_PAUSE:
			state->game_state = RUNNING;
			break;
		}
	}
}

int check_unique(char& symb1, char& symb2, char& symb3)
{
	return !(symb1 == symb2 && symb1 == symb3 && symb2 == symb3);
}

int main(int argc, char *argv[])
{
	/* game objects */
	board board;
	gobject snake;
	gobject fruit;
	score score;
	state state;
	settings settings;

	/* set default game settings */
	settings.border_length = BOARD_LENGTH;
	settings.border_width = BOARD_WIDTH;
	settings.border_thickness = BOARD_BORDER_THICKNESS;
	settings.border_symbol = BOARD_BORDER_SYMBOL;
	settings.snake_symbol = SNAKE_SYMBOL;
	settings.fruit_symbol = FRUIT_SYMBOL;

	/* check symbols are unique */
	if (check_unique(settings.fruit_symbol, settings.snake_symbol,
			settings.border_symbol) == 0)
	{
		std::cout << "Symbole sa takie same" << "\n";
		return -1;
	}

	/* game preparation */
	gobject_init(&snake);
	gobject_init(&fruit);
	reset(&board, &snake, &fruit, &score, &state, &settings);
	draw(&board, &snake, &fruit, &score);

	/* game start */
	while (state.game_state != QUIT)
	{
		input(&state);

		switch (state.game_state)
		{
		case RUNNING:
			logic(&board, &snake, &fruit, &score, &state);
			draw(&board, &snake, &fruit, &score);
			break;
		case CRASH:
			reset(&board, &snake, &fruit, &score, &state, &settings);
			break;
		case PAUSE:
			break;
		case QUIT:
			break;
		}
	}

	/* clean up */
	gobject_destory(&snake);
	gobject_destory(&fruit);

	return 0;
}
