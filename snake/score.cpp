/**
 * score.c
 *
 * Scoring system.
 *
 * @author: Freddie Haddad
 */

#include "score.hpp"

void score_clear(score *score) {
  score->score = 0;
}

void score_update(score *score, int value) {
  score->score += value;
}

int score_current(const score *score) {
  return score->score;
}
