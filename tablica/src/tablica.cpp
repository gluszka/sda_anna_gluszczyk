//============================================================================
// Name        : tablica.cpp
// Author      : 
// Version     :
// Copyright   : Your copyright notice
// Description : Hello World in C++, Ansi-style
//============================================================================

#include <iostream>
using namespace std;

int minimum(const int tablica[], int rozmiar)
{
	int temp = tablica[0];

	for (int i = 1; i < rozmiar; i++)
	{
		if (temp > tablica[i])
			temp = tablica[i];
	}
	return temp;
}

int main()
{
const int dane[] =
{ 4, 7, 2, -2, 3, 0, 2, -1 };

cout << "Najmniejszą liczbą jest " << minimum(dane, 8) << endl;

return 0;
}
