/*
 * CalculatorTest2.cpp
 *
 *  Created on: 20.04.2017
 *      Author: RENT
 */


#include "gtest/gtest.h"
#include "Calculator 2.hpp"


TEST(CalculatorTest, ConstuctorTest) {
      Calculator c(2,3);
      EXPECT_EQ(0, c.getResult());
}

TEST(CalculatorTest, AddTest) {
      Calculator c(2,3);
      EXPECT_EQ(5, c.add());
      EXPECT_EQ(5, c.getResult());
}


int main(int argc, char **argv) {
      ::testing::InitGoogleTest(&argc, argv);

      return RUN_ALL_TESTS();
}

