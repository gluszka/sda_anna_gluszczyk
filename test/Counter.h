/*
 * Counter.h
 *
 *  Created on: 19.04.2017
 *      Author: RENT
 */

#ifndef COUNTER_H_
#define COUNTER_H_

class Counter {
private:
      int mCounter;
public:
      Counter() : mCounter(0) {}
      int Increment();
};




#endif /* COUNTER_H_ */
