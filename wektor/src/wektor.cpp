//============================================================================
// Name        : wektor.cpp
// Author      : 
// Version     :
// Copyright   : Your copyright notice
// Description : Hello World in C++, Ansi-style
//============================================================================
#include <iostream>
#include <cmath>
#include <iomanip>



struct Vector3D

{
	double x, y, z;
};

Vector3D nowyWektor(double x, double y, double z)

{
	return Vector3D { x, y, z };
}

Vector3D operator+ (Vector3D a ,Vector3D b)
{
	return nowyWektor (a.x +b.x, a.y +b.y, a.z +b.z);
}

Vector3D operator- (Vector3D a ,Vector3D b)
{
	return nowyWektor (a.x -b.x, a.y -b.y, a.z -b.z);
}
Vector3D operator* (int i , Vector3D b)
{
	return nowyWektor (i* b.x, i*b.y, i*b.z);
}
Vector3D operator* (Vector3D a ,int i)
{
	return nowyWektor (i* a.x, i*a.y, i*a.z);
}
Vector3D operator- (Vector3D a)
{
	return -1*a;
}

std::ostream& operator<<(std::ostream& out, const Vector3D& v)

{

	std::streamsize precyzja = out.precision();  // zapisuję starą precyzję

	std::ios_base::fmtflags flagi = out.flags(); // zapisuję stan flag

	out << std::setprecision(4) << std::fixed << '[' << v.x << ", " << v.y

			<< ", " << v.z << ']';

	out.flags(flagi);        // odtworzenie zapisanych flag

	out.precision(precyzja); // odtworzenie zapisanej precyzji

	return out;

}


int main(int argc, char* argv[])

{

	Vector3D v = nowyWektor(1.0, 2.0, 3.0);

	Vector3D u = nowyWektor(4.0, 5.0, 6.0);

	std::cout << "v = " << v << ",\nu = " << u << std::endl;



//	Vector3D vn = normalizujWektor(v);

//	Vector3D un = normalizujWektor(u);

//	std::cout << "Znormalizowane:\nv = " << vn << ",\nu = " << un << std::endl;



	Vector3D w;

	double s = 0.0;



	w = 3 * v;

	std::cout << "Mnożenie przez skalar #1: " << w << std::endl;



	w = v * 3;

	std::cout << "Mnożenie przez skalar #2: " << w << std::endl;



	w = -v;

	std::cout << "Wektor przeciwny: " << w << std::endl;



	w = v + u;

	std::cout << "Suma: " << w << std::endl;



	w = v + (-v);

	std::cout << "Suma z wektorem przeciwnym: " << w << std::endl;



	w = v - u;

	std::cout << "Różnica: " << w << std::endl;



//	w = v * u;

	std::cout << "Iloczyn wektorowy: " << w << std::endl;



//	w = normalizujWektor(v * u);

	std::cout << "Iloczyn wektorowy znormalizowany: " << w << std::endl;



//	w = normalizujWektor(vn * un);

	std::cout << "Znormalizowany iloczyn wektorów znormalizowanych: " << w << std::endl;



//	s = v ^ u;

	std::cout << "Iloczyn skalarny: " << s << std::endl;



	return 0;

}
